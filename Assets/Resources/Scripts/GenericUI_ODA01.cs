﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class GenericUI_ODA01 : MonoBehaviour {

	//Scripts Ayuda
	Reto1_ODA01 reto1Script;
	Reto2_ODA01 reto2Script;
	Reto3_ODA01 reto3Script;
	Reto4_ODA01 reto4Script;
	//RetoFinal_ODA09 retoFinalScript;*/
	[HideInInspector]
	public SoundManager soundManager;


	//Contenedores
	[HideInInspector]
	public GameObject genericUI, gameUI, herramientas;

	//BlockScreens
	GameObject imgBlockScreen;

	//PopUps
	GameObject popUpInfo, popUp2Button;

	//Botones popUp
	Button btnAceptar, btnMochila; //btnSi, btnNo;

	//Textos popUp
	Text txtInfo; //txt2Button;

	//Herramientas
	public enum Herramientas
	{
		pinceles,
		vaso,
		godete,
		pinturas,
		lapiz,
		letras,
		goma,
		sacapuntas,
		libreta 
	}

	Dictionary<String, int> herramientasObtenidas;

	// Use this for initialization
	void Start () {
		//Control de herramientas
		herramientasObtenidas = new Dictionary<String, int> ();

		//Scripts Ayuda
		reto1Script = transform.GetComponent<Reto1_ODA01>();
		reto2Script = transform.GetComponent<Reto2_ODA01>();
		reto3Script = transform.GetComponent<Reto3_ODA01>();
		reto4Script = transform.GetComponent<Reto4_ODA01>();
		//retoFinalScript = transform.GetComponent<RetoFinal_ODA09>();
		soundManager = transform.GetComponent<SoundManager>();

		//Contenedores
		genericUI = transform.FindChild("GenericUI").gameObject;
		gameUI = genericUI.transform.FindChild("gameUI").gameObject;
		herramientas = gameUI.transform.FindChild ("herramientas").gameObject;

		//BlockScreens
		imgBlockScreen = genericUI.transform.FindChild("imgBlockScreen").gameObject;

		//PopUps
		popUpInfo = imgBlockScreen.transform.FindChild("popUpInfo").gameObject;
		popUp2Button = imgBlockScreen.transform.FindChild("popUp2Button").gameObject;

		//Botones popUp
		btnAceptar = popUpInfo.transform.FindChild("btnAceptar").GetComponent<Button>();
		//btnSi = popUp2Button.transform.FindChild("btnSi").GetComponent<Button>();
		//btnNo = popUp2Button.transform.FindChild("btnNo").GetComponent<Button>();
		btnMochila = gameUI.transform.FindChild ("mochila").GetComponent<Button>();

		//Texttos popUP
		txtInfo = popUpInfo.transform.FindChild("txtInfo").GetComponent<Text>();
		//txt2Button = popUp2Button.transform.FindChild("txt2Button").GetComponent<Text>();
			
		//soundManager.playSound ("Sound_CarrosCalle", 0);

		checarHerramientasGanadas ();

		btnMochila.onClick.AddListener(delegate() {
			mostrarOcultarMochila();
		});
	}

	//Funciones secuencia
	public IEnumerator eventosUI (string evento, float tiempo)
	{
		yield return new WaitForSeconds (tiempo);
		string[] instrucciones = evento.Split ('_');
		switch (instrucciones [0]) {
		case "openPopUp":
			openPopUp (instrucciones [1]);
			break;
		case "closePopUp":
			closePopUp (instrucciones [1]);
			break;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(herramientas.activeInHierarchy)
			actualizarMochila ();
	}

	void openPopUp (string popUpName)
	{
		imgBlockScreen.SetActive (true);
		switch (popUpName) {
		case "popUpInfo":
			popUpInfo.gameObject.SetActive (true);
			break;
		case "popUp2Button":
			popUp2Button.gameObject.SetActive (true);
			break;
		}

		soundManager.playSound ("Sound_OpenPopUp_10",1);

	}

	void closePopUp (string popUpName)
	{
		switch(popUpName){
		case "popUpInfo":
			popUpInfo.gameObject.SetActive (false);
			break;
		case "popUp2Button":
			popUp2Button.gameObject.SetActive (false);
			break;
		}
		imgBlockScreen.SetActive (false);
		soundManager.playSound ("Sound_Cerrar_3",1);

	}

	public void irMenu()
	{
		closePopUp ("popUpInfo");
		closePopUp ("popUp2Button");
		gameUI.SetActive (false);
		this.GetComponent<Menu_ODA01> ().abrirMenu ();
		soundManager.stopSound (0);
		Destroy (this.GetComponent<Menu_ODA01> ().retoActivo);
	}

	/*public void reiniciarEscena(int reto, int escena)
	{
		closePopUp ("popUpInfo");
		closePopUp ("popUp2Button");

		switch(reto)
		{
		case 1:
			//reto1Script.avanzarEscena (escena);//instanciaODA.GetComponent<Reto1_ODA09>().avanzarEscena (escena);
			break;
		case 2:
			//reto2Script.avanzarEscena (escena);//instanciaODA.GetComponent<Reto2_ODA09>().avanzarEscena (escena);
			break;
		case 3:
			//reto3Script.avanzarEscena (escena);//instanciaODA.GetComponent<Reto3_ODA09>().avanzarEscena (escena);
			break;
		}
	}*/

	//Muestra popUp de información y asigna la funcion indicada en el boton, del reto indicado
	public void mostrarInfoPopUp(string texto, string funcion, int reto, float tiempoEspera)
	{
		//Habilita los gameObject del popUp
		imgBlockScreen.SetActive (true);
		popUpInfo.SetActive (true);
		soundManager.playSound ("Sound_OpenPopUp_10",1);

		//Asigna el texto al popUp
		txtInfo.text = texto;

		//Remueve todas las funciones anteriores del boton
		btnAceptar.onClick.RemoveAllListeners ();

		switch(reto)
		{
		case 0:
			btnAceptar.onClick.AddListener (delegate() {
				StartCoroutine(eventosUI(funcion, 0.01f));
				closePopUp ("popUpInfo");
			});
			break;
		case 1:
			btnAceptar.onClick.AddListener (delegate() {
				StartCoroutine(reto1Script.secuenciaEventos(funcion,tiempoEspera));
				closePopUp("popUpInfo");
			});
			break;
		case 2:
			btnAceptar.onClick.AddListener (delegate() {
				StartCoroutine(reto2Script.secuenciaEventos(funcion,tiempoEspera));
				closePopUp("popUpInfo");
			});
			break;
		case 3:
			btnAceptar.onClick.AddListener (delegate() {
				StartCoroutine(reto3Script.secuenciaEventos(funcion,tiempoEspera));
				closePopUp("popUpInfo");
			});
			break;
		case 4:
			btnAceptar.onClick.AddListener (delegate() {
				StartCoroutine(reto4Script.secuenciaEventos(funcion,tiempoEspera));
				closePopUp("popUpInfo");
			});
			break;
		}

	}

	/*
	//Muestra popUp de 2 botones, asigna el texto y las funciones indicadas del reto indicado
	public void mostrar2ButtonPopUp(string texto, string funcNo, string funcSi, string txtBtnNo, string txtBtnSi, int reto)
	{
		//Habilita los gameObject del popUp
		imgBlockScreen.SetActive (true);
		popUp2Button.SetActive (true);


		//Asigna el texto al popUp
		txt2Button.text = texto;

		//Texto de los botones
		btnSi.transform.GetChild(0).GetComponent<Text>().text = txtBtnSi;
		btnNo.transform.GetChild(0).GetComponent<Text>().text = txtBtnNo;

		//Remueve todas las funciones anteriores del boton
		btnSi.onClick.RemoveAllListeners ();
		btnNo.onClick.RemoveAllListeners ();

		switch(reto)
		{
		case 1:
			btnSi.onClick.AddListener (delegate() {
				//StartCoroutine(reto1Script.secuenciaEventos(funcSi, 1.0f));
				closePopUp("popUp2Button");
			});

			btnNo.onClick.AddListener (delegate() {
				//StartCoroutine(reto1Script.secuenciaEventos(funcNo, 1.0f));
				closePopUp("popUp2Button");
			});
			break;
		case 2:
			btnSi.onClick.AddListener (delegate() {
				//StartCoroutine(reto2Script.secuenciaEventos(funcSi, 1.0f));
				closePopUp("popUp2Button");
			});

			btnNo.onClick.AddListener (delegate() {
				//StartCoroutine(reto2Script.secuenciaEventos(funcNo, 1.0f));
				closePopUp("popUp2Button");
			});
			break;
		case 3:
			btnSi.onClick.AddListener (delegate() {
				//StartCoroutine(reto3Script.secuenciaEventos(funcSi, 1.0f));
				closePopUp("popUp2Button");
			});

			btnNo.onClick.AddListener (delegate() {
				//StartCoroutine(reto3Script.secuenciaEventos(funcNo, 1.0f));
				closePopUp("popUp2Button");
			});
			break;
		case 4:
			btnSi.onClick.AddListener (delegate() {
				//StartCoroutine(retoFinalScript.secuenciaEventos(funcSi, 1.0f));
				closePopUp("popUp2Button");
			});

			btnNo.onClick.AddListener (delegate() {
				//StartCoroutine(retoFinalScript.secuenciaEventos(funcNo,1.0f));
				closePopUp("popUp2Button");
			});
			break;
		}
	}*/

	void checarHerramientasGanadas()
	{
		foreach (string herramienta in Enum.GetNames(typeof(Herramientas))) 
		{
			if (!PlayerPrefs.HasKey (herramienta)) {
				PlayerPrefs.SetInt (herramienta, 0);	
			}
			if (!herramientasObtenidas.ContainsKey (herramienta)) {
				herramientasObtenidas.Add (herramienta, PlayerPrefs.GetInt (herramienta));
			} else {
				herramientasObtenidas[herramienta] = PlayerPrefs.GetInt (herramienta);
			}
		}
	}

	public void ganarHerramienta(Herramientas herramienta)
	{
		imgBlockScreen.SetActive (true);
		imgBlockScreen.transform.FindChild ("herramientas").FindChild(herramienta.ToString()).
			GetComponent<Animator>().Play("herramientaTamanio");
		PlayerPrefs.SetInt (herramienta.ToString(), 1);
		if (herramientasObtenidas.ContainsKey (herramienta.ToString ())) {
			herramientasObtenidas [herramienta.ToString ()] = 1;
		} else {
			herramientasObtenidas.Add (herramienta.ToString (), 1);
		}
		StartCoroutine (eventosUI("closePopUp_nada", 2.2f));
	}

	void actualizarMochila()
	{
		
		foreach (Transform herramienta in herramientas.transform) 
		{
			if (herramientasObtenidas.ContainsKey (herramienta.name)) {
				herramienta.GetComponent<Animator> ().SetInteger ("activo", herramientasObtenidas [herramienta.name]);
			} else {
				herramienta.GetComponent<Animator> ().SetInteger ("activo",0);
			}
		}
	}

	public void mostrarOcultarMochila()
	{
		GameObject mochila = herramientas.gameObject;
		mochila.SetActive(!mochila.activeInHierarchy);
		if(mochila.activeInHierarchy)
			actualizarMochila ();
	}

	public void reiniciarHerramientas()
	{
		herramientasObtenidas = new Dictionary<string, int> ();
		foreach (string herramienta in Enum.GetNames(typeof(Herramientas))) 
		{
			PlayerPrefs.SetInt (herramienta, 0);	
			herramientasObtenidas.Add (herramienta, PlayerPrefs.GetInt (herramienta));
		}
	}

	public void parpadearMochila()
	{
		btnMochila.gameObject.GetComponent<Animator> ().Play ("mochilaParpadea");
	}
}
