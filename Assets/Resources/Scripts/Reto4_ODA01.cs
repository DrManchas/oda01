﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class Reto4_ODA01 : MonoBehaviour {

	//Scripts ayuda
	GenericUI_ODA01 genericUIScript;
	Menu_ODA01 menuScript;

	public GameObject retoActivo;
	Transform opcionActiva;

	bool anuncioValido = false;

	int marco = 1;
	int color = 1;

	// Use this for initialization
	void Start () {

		//Scripts de Ayuda
		genericUIScript = this.transform.GetComponent<GenericUI_ODA01>();
		menuScript = this.transform.GetComponent<Menu_ODA01>();
	}

	// Update is called once per frame
	void Update () {
		if (!anuncioValido && retoActivo != null) {
			
			validarAnuncioTerminado ();
		}
	}

	// ------------------------Genericos Retos----------------------

	//Iniciar clone
	public void inicarReto (GameObject reto){

		retoActivo = reto;

		anuncioValido = false;

		genericUIScript.soundManager.stopSound (0);

		//seleccionar anuncio
		seleccionarAnuncio();

		//Indicaciones
		StartCoroutine(secuenciaEventos("openPopUp_introduccion", 1.0f));

	}

	//Funciones secuencia
	public IEnumerator secuenciaEventos (string secuencia, float tiempoEspera)
	{
		yield return new WaitForSeconds (tiempoEspera);
		string[] instrucciones = secuencia.Split ('_');
		switch (instrucciones[0]) {
		case "openPopUp":
			openPopUp (instrucciones[1]);
			break;
		case "closePopUp":
			closePopUp (instrucciones[1]);
			break;
		case "activarEspacioTrabajo":
			activarEspacioTrabajo ();
			break;
		case "guardarAnuncio":
			genericUIScript.gameUI.SetActive (true);
			genericUIScript.parpadearMochila ();
			break;
		}

	}

	//Eventos al abrir popUp

	public void openPopUp (string popUpName)
	{
		//genericUIScript.soundManager.playSound ("Sound_OpenPopUp 10",1);
		Button btnPopUp = retoActivo.transform.FindChild ("imgBlockScreen").FindChild("btnAceptar").GetComponent<Button>();;
		switch(popUpName){
		case "introduccion":
			retoActivo.transform.FindChild ("imgBlockScreen").gameObject.SetActive (true);
			btnPopUp.onClick.RemoveAllListeners ();
			btnPopUp.onClick.AddListener(delegate() {
				closePopUp("introduccion");
			});
			break;
		case "frase":
			Transform popUp = retoActivo.transform.FindChild ("imgBlockScreen").FindChild ("popUp");
			popUp.FindChild ("Text").gameObject.SetActive(false);
			popUp.FindChild ("frases").FindChild(opcionActiva.name).gameObject.SetActive(true);
			btnPopUp.onClick.RemoveAllListeners ();
			btnPopUp.onClick.AddListener(delegate() {
				closePopUp("frase");
			});
			break;
		case "pintura":
			genericUIScript.gameUI.SetActive (false);
			genericUIScript.mostrarInfoPopUp (
				"Todavía te quedan unas herramientas: escoge el color de la pintura y el pincel que quieres usar para enmarcar " +
				"tu anuncio y darle un estilo propio.",
				"closePopUp_pintura",
				4,
				1.20f
			);
			break;
		case "terminarReto":
			tomarScreenShot ();
			genericUIScript.soundManager.playSound ("TerminarReto",1);
			genericUIScript.mostrarInfoPopUp (
				"¡Felicidades! Gracias a tu anuncio la comunidad de Ítaca sabe que se renovarán los servicios de salud y se volverá a poblar." +
				" ¡Sigamos con el reto final!",
				"closePopUp_terminarReto",
				4,
				0.0f
			);
			break;
		}

	}

	//Eventos al cerrar popUp
	void closePopUp (string popUpName)
	{
		switch(popUpName){
		case "introduccion":
			StartCoroutine (secuenciaEventos("openPopUp_frase",0.5f));
			break;
		case "frase":
			retoActivo.transform.FindChild ("imgBlockScreen").gameObject.SetActive(false);
			genericUIScript.gameUI.SetActive (true);
			genericUIScript.parpadearMochila ();
			StartCoroutine (secuenciaEventos("activarEspacioTrabajo",1.0f));
			break;
		case "pintura":
			genericUIScript.gameUI.SetActive (true);
			genericUIScript.parpadearMochila ();
			StartCoroutine( mostrarPinturas ());
			break;
		case "terminarReto":
			Destroy (retoActivo);
			retoActivo = null;
			menuScript.abrirReto (4);
			break;
		}
	}

	// ------------------------Individuales Reto----------------------

	// ------------------------Vista---------------------------------

	void tomarScreenShot()
	{
		StartCoroutine(ScreenshotEncode()); 
	}

	IEnumerator mostrarPinturas ()
	{
		yield return new WaitForSeconds(1.0f);
		genericUIScript.gameUI.SetActive (false);

		retoActivo.transform.FindChild ("espacioTrabajo").FindChild("anuncio").GetChild(0).gameObject.SetActive(true);

		Transform pinturas = retoActivo.transform.FindChild ("espacioTrabajo").FindChild ("pinturas");
		pinturas.gameObject.SetActive (true);

		pinturas.GetChild (0).GetComponent<Button>().onClick.AddListener(delegate() {
			pintarMarco(pinturas.GetChild (0).name);
		});

		pinturas.GetChild (1).GetComponent<Button>().onClick.AddListener(delegate() {
			pintarMarco(pinturas.GetChild (1).name);
		});

		pinturas.GetChild (2).GetComponent<Button>().onClick.AddListener(delegate() {
			pintarMarco(pinturas.GetChild (2).name);
		});

		pinturas.GetChild (3).GetComponent<Button>().onClick.AddListener(delegate() {
			pintarMarco(pinturas.GetChild (3).name);
		});

		pinturas.GetChild (4).GetComponent<Button>().onClick.AddListener(delegate() {
			pintarMarco(pinturas.GetChild (4).name);
		});

		pinturas.GetChild (5).GetComponent<Button>().onClick.AddListener(delegate() {
			pintarMarco(pinturas.GetChild (5).name);
		});

		Button guardar = retoActivo.transform.FindChild ("btnGuardar").GetComponent<Button>();
		guardar.onClick.RemoveAllListeners ();
		guardar.onClick.AddListener(delegate() {
			openPopUp("terminarReto");
		});
	}

	void pintarMarco(string pintura)
	{
		string[] instrucciones = pintura.Split ('_');
		switch (instrucciones [0]) {
		case "pincel":
			marco = int.Parse(instrucciones [1]);
			break;
		case "pintura":
			color = int.Parse(instrucciones [1]);
			break;
		}
		Transform marcos = retoActivo.transform.FindChild ("espacioTrabajo").FindChild("anuncio").GetChild(0);

		foreach (Transform colores in marcos) 
		{
			colores.gameObject.SetActive (false);
		}

		Transform transformColor = marcos.GetChild (color - 1);
		transformColor.gameObject.SetActive (true);

		foreach (Transform formas in transformColor) 
		{
			formas.gameObject.SetActive (false);
		}

		transformColor.GetChild (marco-1).gameObject.SetActive (true);
	}

	void activarEspacioTrabajo()
	{
		try
		{
			Transform espacioTrabajo = retoActivo.transform.FindChild("espacioTrabajo");
			espacioTrabajo.gameObject.SetActive(true);

			espacioTrabajo.FindChild("anuncio").gameObject.AddComponent<DropReto4>();
			espacioTrabajo.FindChild("anuncio").tag = "fondo";
			espacioTrabajo.FindChild("titulo").gameObject.AddComponent<DropReto4>();
			espacioTrabajo.FindChild("titulo").tag = "titulo";;

			for(int index= 1; index <= 4; index++)
			{
				opcionActiva.transform.FindChild("titulo"+index).gameObject.AddComponent<DragReto4>();
				opcionActiva.transform.FindChild("titulo"+index).tag = "titulo";
				opcionActiva.transform.FindChild("imagen"+index).gameObject.AddComponent<DragReto4>();
				opcionActiva.transform.FindChild("imagen"+index).tag = "fondo";
			}

			genericUIScript.gameUI.SetActive(false);

		} catch {
			Debug.Log ("Error asignando valores de la pregunta");
		}

	}
		

	// ------------------------Logica---------------------------------

	void seleccionarAnuncio()
	{
		try{
			Transform opciones = retoActivo.transform.FindChild("espacioTrabajo").FindChild("opciones");

			int randomOpcion = Random.Range(0,opciones.childCount);
			opcionActiva = opciones.GetChild(randomOpcion);
			opcionActiva.gameObject.SetActive(true);

		} catch {
			Debug.Log ("Error seleccionando pregunta");	
		}

	}

	public void validarAnuncioTerminado()
	{
		Transform espacioTrabajo = retoActivo.transform.FindChild("espacioTrabajo");

		if (espacioTrabajo.FindChild("anuncio").GetComponent<Image>().sprite != null 
			&& espacioTrabajo.FindChild("titulo").GetComponent<Image>().sprite != null
			&& espacioTrabajo.FindChild("eslogan").FindChild("Text").GetComponent<Text>().text.Length > 0) 
		{
			anuncioValido = true;
			Button guardar = retoActivo.transform.FindChild ("btnGuardar").GetComponent<Button>();
			guardar.gameObject.SetActive (true);
			guardar.onClick.AddListener(delegate() {
				openPopUp("pintura");
			});
		}
	}

	//-----------Script ScreenShot ---------------//

	Texture2D texture1;
	private static AndroidJavaObject _activity;


	private const string MediaStoreImagesMediaClass = "android.provider.MediaStore$Images$Media";

	public static string SaveImageToGallery(Texture2D texture2D, string title, string description)
	{
		using (var mediaClass = new AndroidJavaClass(MediaStoreImagesMediaClass))
		{
			using (var cr = Activity.Call<AndroidJavaObject>("getContentResolver"))
			{
				var image = Texture2DToAndroidBitmap(texture2D);
				var imageUrl = mediaClass.CallStatic<string>("insertImage", cr, image, title, description);
				return imageUrl;
			}
		}
	}

	public static AndroidJavaObject Texture2DToAndroidBitmap(Texture2D texture2D)
	{
		byte[] encoded = texture2D.EncodeToPNG();
		using (var bf = new AndroidJavaClass("android.graphics.BitmapFactory"))
		{
			return bf.CallStatic<AndroidJavaObject>("decodeByteArray", encoded, 0, encoded.Length);
		}
	}

	public static AndroidJavaObject Activity
	{
		get
		{
			if (_activity == null)
			{
				var unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				_activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
			}
			return _activity;
		}
	}


	IEnumerator ScreenshotEncode() {

		yield return new WaitForEndOfFrame ();
		texture1 = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, false);
		texture1.ReadPixels (new Rect (0, 0, Screen.width, Screen.height), 0, 0);
		texture1.Apply ();
		SaveImageToGallery (texture1, "CapturaOda1", "");
	}

}



public class DragReto4 : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

	Vector3 posicionOriginal;

	public void OnBeginDrag(PointerEventData eventData)
	{
		posicionOriginal = transform.localPosition;

		//Se agrupa con el cursor
		if(gameObject.GetComponent<CanvasGroup>() == null){
			gameObject.AddComponent<CanvasGroup>();
		}

		//Se indica que no estorbe los ray casts
		gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;

	}

	public void OnDrag(PointerEventData eventData)
	{
		Vector3 globalMousePos;
		RectTransformUtility.ScreenPointToWorldPointInRectangle (
			eventData.pointerEnter.transform as RectTransform, eventData.position, 
			eventData.pressEventCamera, out globalMousePos
		);
		transform.position = globalMousePos;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		transform.localPosition = posicionOriginal;
		gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;
	}
}

public class DropReto4 : MonoBehaviour, IDropHandler
{

	public void OnDrop(PointerEventData data)
	{
		if (transform.tag == data.pointerDrag.tag) {
			//Obtiene imagen del objeto que se tiro en este objeto
			Sprite dropSprite = GetDropSprite (data);
			//Si es la respuesta correcta se le cambia la imagen
			if (dropSprite != null) {
				this.GetComponent<Image> ().sprite = dropSprite;
				this.GetComponent<Image> ().color = Color.white;
			}
		}
	}

	private Sprite GetDropSprite(PointerEventData data)
	{
		//Se obtiene el objeto que se esta arrastrando, si no existe se sale
		var originalObj = data.pointerDrag;
		if (originalObj == null)
			return null;


		//Se obtiene la imagen del objeto que se arrastra, si es nulo se regresa nulo
		var srcImage = originalObj.GetComponent<Image>();
		if (srcImage == null)
			return null;

		return srcImage.sprite;
	}
}