﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class RetoFinal_ODA01 : MonoBehaviour {

	//Scripts ayuda
	GenericUI_ODA01 genericUIScript;
	Menu_ODA01 menuScript;

	//RetoActivo
	public GameObject retoActivo = null;

	//popUps
	List<GameObject> popUps = new List<GameObject>();

	//popUpsButtons
	Button btnAtras, btnAceptar, btnSalir;
	List<Button> btnRespuestas = new List<Button>();

	Text txtNumPregunta;
	Text txtCalificacion;
	GameObject respuestaCorrecta;
	GameObject respuestaIncorrecta;

	//Variables de evaluacion
	List<PreguntaRetoFinal> preguntas = new List<PreguntaRetoFinal>();
	List<RespuestaRetoFinal> respuestas = new List<RespuestaRetoFinal>();
	int preguntaActual = 0;

	// ------------------------Genericos Unity----------------------
	void Start ()
	{

		//Scripts de Ayuda
		genericUIScript = this.transform.GetComponent<GenericUI_ODA01>();
		menuScript = this.transform.GetComponent<Menu_ODA01>();

	}

	void Update(){
		
	}

	// ------------------------Genericos Retos----------------------

	//Iniciar clone
	public void inicarReto (GameObject reto){

		//Relacionar objetos
		retoActivo = reto;

		//Generar preguntas
		crearCuestionario ();

		//seleccionar anuncio
		seleccionarPreguntasRandom(5);

		genericUIScript.soundManager.playSound ("Sound_Fondoevaluacion", 0);

		//popUps
		popUps.Add(retoActivo.transform.FindChild ("preguntas").gameObject);
		popUps.Add(retoActivo.transform.FindChild ("resultados").gameObject);

		//popUps Buttons
		btnAtras = popUps[0].transform.FindChild("btnAtras").GetComponent<Button>();
		btnAceptar = popUps [0].transform.FindChild ("btnSiguiente").GetComponent<Button> ();

		btnSalir = popUps[1].transform.FindChild("btnSalir").GetComponent<Button>();

		txtNumPregunta = popUps[0].transform.FindChild("txtNumPregunta").GetComponent<Text>();
		txtCalificacion = popUps[1].transform.FindChild("txtCalificacion").GetComponent<Text>();
		respuestaCorrecta = popUps[1].transform.FindChild("imgCorrecto").gameObject;
		respuestaIncorrecta = popUps[1].transform.FindChild("imgIncorrecto").gameObject;

		preguntaActual = 0;

		popUps [0].SetActive (true);
		popUps [1].SetActive (false);

		//Funciones Botones
		btnAtras.onClick.AddListener (delegate() {
			regresar();
		});
		btnAceptar.onClick.AddListener (delegate() {
			avanzar();
		});

		btnSalir.onClick.AddListener (delegate() {
			limpiarValores ();
			menuScript.abrirMenu ();
		});
			
		mostrarPregunta ();
	}

	//Funciones secuencia
	public IEnumerator secuenciaEventos (string secuencia, float tiempoEspera)
	{
		yield return new WaitForSeconds (tiempoEspera);
		switch (secuencia) {
		case "evaluar":
			mostrarResultados ();
			break;
		case "salir":
			limpiarValores ();
			menuScript.abrirMenu ();
			break;
		}

	}

	// ------------------------Funciones UI------------------------------

	void limpiarValores()
	{
		//popUps
		popUps = new List<GameObject>();

		//popUpsButtons
		btnAtras = null; 
		btnAceptar = null;
		btnSalir = null;
		btnRespuestas = new List<Button>();

		txtCalificacion = null;
		respuestaCorrecta = null;
		respuestaIncorrecta = null;

		//Variables de evaluacion
		preguntas = new List<PreguntaRetoFinal>();
		respuestas = new List<RespuestaRetoFinal>();

		Destroy (retoActivo);
		retoActivo = null;

		genericUIScript.reiniciarHerramientas ();
	}

	void asignarValores()
	{
		ocultarPreguntas ();
		PreguntaRetoFinal pregunta = respuestas [preguntaActual].GetQuestion ();
		pregunta.GetPreguntaGO ().SetActive (true);

		btnRespuestas = new List<Button> ();
		btnRespuestas.Add (pregunta.GetRespuestasGO()[0]);
		btnRespuestas.Add (pregunta.GetRespuestasGO()[1]);
		btnRespuestas.Add (pregunta.GetRespuestasGO()[2]);

		btnRespuestas[0].onClick.AddListener (delegate() {
			seleccionarRespuesta(0);
		});
		btnRespuestas[1].onClick.AddListener (delegate() {
			seleccionarRespuesta(1);
		});
		btnRespuestas[2].onClick.AddListener (delegate() {
			seleccionarRespuesta(2);
		});

		ocultarExtras ();

		if (pregunta.GetExtraGO () != null) 
		{
			pregunta.GetExtraGO ().SetActive (true);
		}

		txtNumPregunta.text = ""+(preguntaActual+1)+".";
		RectTransform txtPregunta = (RectTransform)pregunta.GetPreguntaGO ().transform.FindChild ("pregunta");
		txtNumPregunta.transform.localPosition = 
			new Vector3 (txtPregunta.localPosition.x-(txtPregunta.rect.width/2)-15 , txtPregunta.localPosition.y);
		txtNumPregunta.rectTransform.sizeDelta = new Vector2(50,txtPregunta.rect.height); 
	}

	void ocultarPreguntas()
	{
		for (int index = 0; index < respuestas.Count; index++) 
		{
			respuestas [index].GetQuestion ().GetPreguntaGO ().SetActive (false);
		}
	}

	void ocultarExtras()
	{
		Transform extras = popUps[0].transform.FindChild("extraInfo");
		foreach (Transform child in extras) 
		{
			child.gameObject.SetActive (false);
		}
	}

	void mostrarPregunta(){
		
		asignarValores ();

		colorearRespuestaSeleccionada ();

		if (preguntaActual <= 0) {
			btnAtras.interactable = false;
		} else {
			btnAtras.interactable = true;
			btnAtras.transform.GetChild (0).GetComponent<Text> ().text = "ANTERIOR";
		}

		if (preguntaActual >= respuestas.Count-1) {
			btnAceptar.transform.GetChild (0).GetComponent<Text> ().text = "TERMINAR";
		} else {
			btnAceptar.transform.GetChild (0).GetComponent<Text> ().text = "SIGUIENTE";
		}
	}

	void colorearRespuestaSeleccionada()
	{
		for (int index = 0; index < btnRespuestas.Count; index++) 
		{
			if (index == respuestas [preguntaActual].GetAnswer ()) {
				btnRespuestas[index].interactable = false;
			} else {
				btnRespuestas[index].interactable = true;
			}
		}
	}

	void mostrarResultados(){
		popUps [0].SetActive (false);
		popUps [1].SetActive (true);

		int correctas = 0;
		respuestaCorrecta.SetActive(true);
		respuestaIncorrecta.SetActive(true);
		for (int index = 0; index < respuestas.Count; index++) {
			GameObject imgRespuestaClone;


			if (respuestas [index].ReviewQuestion()) {
				imgRespuestaClone = (GameObject) Instantiate(respuestaCorrecta);
				imgRespuestaClone.transform.SetParent(popUps [1].transform);
				imgRespuestaClone.transform.localScale = new Vector3(1, 1, 1);
				imgRespuestaClone.transform.localPosition = respuestaCorrecta.transform.localPosition + (Vector3.down * index * 34);
				correctas++;
			} else {
				imgRespuestaClone = (GameObject) Instantiate(respuestaIncorrecta);
				imgRespuestaClone.transform.SetParent(popUps [1].transform);
				imgRespuestaClone.transform.localScale = new Vector3(1, 1, 1);
				imgRespuestaClone.transform.localPosition = respuestaIncorrecta.transform.localPosition + (Vector3.down * index * 34);
			}
			imgRespuestaClone.name = "resultado";
		}
		respuestaCorrecta.SetActive(false);
		respuestaIncorrecta.SetActive(false);
		txtCalificacion.text = "" + (correctas * 2) + " / 10";
	}

	// ------------------------Funciones Botones-------------------------

	void regresar()
	{
		preguntaActual--;
		mostrarPregunta ();
	}

	void avanzar()
	{
		if (preguntaActual >= respuestas.Count-1)
		{
			StartCoroutine (secuenciaEventos("evaluar",0.1f));
		}
		else
		{
			preguntaActual++;
			mostrarPregunta ();
		}
	}

	void seleccionarRespuesta(int boton)
	{
		respuestas [preguntaActual].SetAnswer (boton);
		genericUIScript.soundManager.playSound ("Sound_SeleccionarRespuesta",1);

		colorearRespuestaSeleccionada ();

	}

	// ------------------------Funciones Evaluacion----------------------

	void seleccionarPreguntasRandom(int numeroPreguntas)
	{

		respuestas = new List<RespuestaRetoFinal> ();
		for (int i = 0; i < numeroPreguntas; i++)
		{
			int randomIndex = Random.Range(0, preguntas.Count);
			respuestas.Add(new RespuestaRetoFinal(preguntas[randomIndex], -1));
			preguntas.RemoveAt(randomIndex);
		}
	}

	void crearCuestionario()
	{

		preguntas = new List<PreguntaRetoFinal>();

		Transform preguntasGO = retoActivo.transform.FindChild ("preguntas");
		for(int index = 0; index<preguntasGO.childCount-4;index++)
		{
			Transform pregunta = preguntasGO.GetChild (index);
			preguntas.Add(new PreguntaRetoFinal(
				pregunta.gameObject,
				new Button[] { 
					pregunta.FindChild("btnRespuesta1").GetComponent<Button>(), 
					pregunta.FindChild("btnRespuesta2").GetComponent<Button>(), 
					pregunta.FindChild("btnRespuesta3").GetComponent<Button>() 
				}));
		}

		//Asignar respuestas 
		preguntas[0].SetRespuestaCorrecta(2);

		preguntas[1].SetRespuestaCorrecta(1);
		preguntas[2].SetRespuestaCorrecta(2);
		preguntas[3].SetRespuestaCorrecta(2);
		preguntas[4].SetRespuestaCorrecta(2);
		preguntas[5].SetRespuestaCorrecta(0);
		preguntas[6].SetRespuestaCorrecta(1);
		preguntas[7].SetRespuestaCorrecta(1);

		//Asignar  objetos extra
		GameObject yogu = preguntasGO.FindChild ("extraInfo").FindChild("imgYogu").gameObject;
		GameObject bebe = preguntasGO.FindChild ("extraInfo").FindChild("imgBebe").gameObject;
		GameObject ninio = preguntasGO.FindChild ("extraInfo").FindChild("imgNino").gameObject;

		preguntas[0].SetExtraGO(null);

		preguntas[1].SetExtraGO(yogu);
		preguntas[2].SetExtraGO(yogu);
		preguntas[3].SetExtraGO(yogu);
		preguntas[4].SetExtraGO(bebe);
		preguntas[5].SetExtraGO(yogu);
		preguntas[6].SetExtraGO(bebe);
		preguntas[7].SetExtraGO(ninio);
	}

}

public class PreguntaRetoFinal
{
	private GameObject _preguntaGO;
	private Button[] _respuestasGO;
	private GameObject _extraGO;
	private int _respuestaCorrecta;

	/// <summary>
	/// Question with only one correct answer
	/// </summary>
	/// <param name="preguntaGO"></param>
	/// <param name="respuestasGO"></param>
	/// 
	public PreguntaRetoFinal(GameObject preguntaGO, Button[] respuestasGO)
	{
		_preguntaGO = preguntaGO;
		_respuestasGO = respuestasGO;
		_extraGO = null;
		_respuestaCorrecta = 0;
	}

	public GameObject GetPreguntaGO()
	{
		return _preguntaGO;
	}

	public Button[] GetRespuestasGO()
	{
		return _respuestasGO;
	}

	public GameObject GetExtraGO()
	{
		return _extraGO;
	}

	public int GetRespuestaCorrecta()
	{
		return _respuestaCorrecta;
	}

	public void SetExtraGO(GameObject valor)
	{
		_extraGO = valor;
	}

	public void SetRespuestaCorrecta(int valor)
	{
		_respuestaCorrecta = valor;
	}

}

public class RespuestaRetoFinal
{
	private PreguntaRetoFinal _question;
	private int _answer;

	/// <summary>
	/// Answer for a single answer question
	/// </summary>
	/// <param name="question">Question object</param>
	/// <param name="answer">Answer to the question object</param>
	public RespuestaRetoFinal(PreguntaRetoFinal question, int answer)
	{
		_question = question;
		_answer = answer;
	}

	public bool ReviewQuestion()
	{
		if (_answer == _question.GetRespuestaCorrecta()) {
			return true;
		} else {
			return false;
		}
	}

	public PreguntaRetoFinal GetQuestion()
	{
		return _question;
	}

	public int GetAnswer()
	{
		return _answer;
	}

	public void SetAnswer(int answer)
	{
		_answer = answer;
	}
}
