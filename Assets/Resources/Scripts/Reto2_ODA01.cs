﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class Reto2_ODA01 : MonoBehaviour {

	//Scripts ayuda
	GenericUI_ODA01 genericUIScript;
	Menu_ODA01 menuScript;

	public GameObject retoActivo;
	PreguntaReto2 preguntaActiva;

	List<PreguntaReto2> preguntas = new List<PreguntaReto2> ();

	int herramientasGanadas = 4;
	bool recorrerCamara = false;

	// Use this for initialization
	void Start () {

		//Scripts de Ayuda
		genericUIScript = this.transform.GetComponent<GenericUI_ODA01>();
		menuScript = this.transform.GetComponent<Menu_ODA01>();
	}

	// Update is called once per frame
	void Update () {
		if (retoActivo != null) {
			recorridoCamara ();
		}
	}

	// ------------------------Genericos Retos----------------------

	//Iniciar clone
	public void inicarReto (GameObject reto){

		retoActivo = reto;

		//Generar preguntas
		generarPreguntas ();

		//seleccionar anuncio
		seleccionarAnuncio();

		//Botones
		Button btnPregunta = retoActivo.transform.FindChild("escenario").FindChild("btnPregunta").GetComponent<Button>();
		btnPregunta.onClick.AddListener(delegate() {
			abrirPregunta();
		});

		Button btnAvanzar = retoActivo.transform.FindChild("avanzar").GetComponent<Button>();
		btnAvanzar.onClick.AddListener(delegate() {
			retoActivo.transform.FindChild ("avanzar").GetComponent<Animator> ().Play ("avanzarDefault");
			recorrerCamara = !recorrerCamara;
		});

		herramientasGanadas = 4;
		recorrerCamara = false;

		genericUIScript.soundManager.playSound ("Sound_aventuraviajereto2", 0);

		//Indicaciones
		StartCoroutine(secuenciaEventos("openPopUp_introduccion", 0.5f));

	}

	//Funciones secuencia
	public IEnumerator secuenciaEventos (string secuencia, float tiempoEspera)
	{
		yield return new WaitForSeconds (tiempoEspera);
		string[] instrucciones = secuencia.Split ('_');
		switch (instrucciones[0]) {
		case "openPopUp":
			openPopUp (instrucciones[1]);
			break;
		case "closePopUp":
			closePopUp (instrucciones[1]);
			break;
		case "reiniciarReto":
			//genericUIScript.reiniciarEscena(1);;
			break;
		}
	}

	//Eventos al abrir popUp

	public void openPopUp (string popUpName)
	{
		genericUIScript.gameUI.SetActive(false);

		switch(popUpName){
		case "introduccion":
			genericUIScript.mostrarInfoPopUp (
				"Muchos habitantes del pueblo se están yendo... Los expertos en salud, política y educación, entre otros, " +
				"no entienden por qué. Avanza por el pueblo, revisa y corrige los anuncios publicitarios, ahí reside la clave.",
				"closePopUp_introduccion",
				2,
				1.20f
			);
			break;
		case "preguntaCorrecta":
			resetPosicionFondo ();
			preguntaCorrecta();
			break;
		case "ganarReto":
			genericUIScript.soundManager.playSound ("TerminarReto",1);
			genericUIScript.mostrarInfoPopUp (
				"Para reflexionar más sobre los mensajes que nos manda la publicidad, ¡sigamos con el tercer reto!",
				"closePopUp_ganarReto",
				2,
				0.00f
			);
			break;
		}

	}

	//Eventos al cerrar popUp
	void closePopUp (string popUpName)
	{
		switch(popUpName){
		case "introduccion":
			Transform avanzar = retoActivo.transform.FindChild ("avanzar");
			avanzar.gameObject.SetActive (true);
			avanzar.GetComponent<Animator> ().Play ("avanzar");
			genericUIScript.gameUI.SetActive (true);
			break;
		case "ganarReto":
			Destroy (retoActivo);
			menuScript.abrirReto (2);
			break;
		}
	}

	// ------------------------Individuales Reto----------------------

	// ------------------------Vista---------------------------------

	void resetPosicionFondo()
	{

		Transform escenario = retoActivo.transform.FindChild ("escenario");
		Vector3 position = escenario.localPosition;
		position.x = 900;
		escenario.localPosition = position;

		Transform avanzar = retoActivo.transform.FindChild ("avanzar");
		avanzar.gameObject.SetActive (true);
		avanzar.GetComponent<Animator> ().Play ("avanzar");

	}

	void recorridoCamara()
	{
		if (recorrerCamara) 
		{
			Transform escenario = retoActivo.transform.FindChild ("escenario");

			if (escenario.localPosition.x < 200) {
				retoActivo.transform.FindChild ("avanzar").gameObject.SetActive (false);
				recorrerCamara = false;
			} else {
				Vector3 position = escenario.localPosition;
				position.x = position.x - 5;
				escenario.localPosition = position;
			}
		}

	}

	void abrirPregunta(){
		retoActivo.transform.FindChild("escenario").FindChild ("pregunta").gameObject.SetActive(true);
		genericUIScript.soundManager.playSound("Fondo_Anuncios",0);
		genericUIScript.gameUI.SetActive(false);

		Transform escenario = retoActivo.transform.FindChild ("escenario");
		Vector3 position = escenario.localPosition;
		position.x = 200;
		escenario.localPosition = position;

		retoActivo.transform.FindChild ("avanzar").gameObject.SetActive (false);
	}

	void preguntaCorrecta()
	{
		retoActivo.transform.FindChild("escenario").FindChild ("pregunta").gameObject.SetActive (false);

		genericUIScript.gameUI.SetActive(true);
		genericUIScript.soundManager.playSound ("Sound_aventuraviajereto2",0);
		genericUIScript.ganarHerramienta ((GenericUI_ODA01.Herramientas) herramientasGanadas);
		herramientasGanadas++;
		if(herramientasGanadas>=9)
		{
			StartCoroutine (secuenciaEventos("openPopUp_ganarReto",2.3f));
		} else {
			seleccionarAnuncio ();
		}
	}

	void asignarValoresPregunta()
	{
		try
		{
			//Desactivar anuncios que no se estan usando
			desactivarAnuncios();

			//Habilitar anuncio seleccionado
			GameObject anuncio = retoActivo.transform.FindChild("escenario").FindChild ("anuncio").
				FindChild("opcion"+preguntaActiva.GetNumOpcion()).gameObject;
			anuncio.SetActive (true);

			GameObject grupoPregunta = retoActivo.transform.FindChild("escenario").FindChild ("pregunta").
				FindChild(""+preguntaActiva.GetNumOpcion()).gameObject;
			grupoPregunta.SetActive (true);

			//Se agrega componente de drop
			preguntaActiva.GetGOTitulo().AddComponent<DropReto2>();
			preguntaActiva.GetGOTitulo().GetComponent<DropReto2>().asignarValores(this.GetComponent<Reto2_ODA01>());
			preguntaActiva.GetGOEslogan().AddComponent<DropReto2>();
			preguntaActiva.GetGOEslogan().GetComponent<DropReto2>().asignarValores(this.GetComponent<Reto2_ODA01>());
			preguntaActiva.GetGOFondo().AddComponent<DropReto2>();
			preguntaActiva.GetGOFondo().GetComponent<DropReto2>().asignarValores(this.GetComponent<Reto2_ODA01>());

			for (int index = 0; index < preguntaActiva.GetGOFondos().Length; index++ )
			{
				preguntaActiva.GetGOFondos()[index].AddComponent<DragReto2>();
				preguntaActiva.GetGOFondos()[index].GetComponent<DragReto2>().respuesta = index+1;
				preguntaActiva.GetGOEslogans()[index].AddComponent<DragReto2>();
				preguntaActiva.GetGOEslogans()[index].GetComponent<DragReto2>().respuesta = index+1;
				preguntaActiva.GetGOTitulos()[index].AddComponent<DragReto2>();
				preguntaActiva.GetGOTitulos()[index].GetComponent<DragReto2>().respuesta = index+1;
			}

		} catch {
			Debug.Log ("Error asignando valores de la pregunta");
		}

	}

	void desactivarAnuncios(){

		Transform anuncios = retoActivo.transform.FindChild("escenario").FindChild ("anuncio");
		foreach (Transform opcion in anuncios) {
			opcion.gameObject.SetActive (false);
		}

		Transform grupoPreguntas = retoActivo.transform.FindChild("escenario").FindChild ("pregunta");
		foreach (Transform pregunta in grupoPreguntas) {
			pregunta.gameObject.SetActive (false);
		}
	}

	// ------------------------Logica---------------------------------

	void seleccionarAnuncio()
	{
		try{
			int randomPregunta = Random.Range(0,preguntas.Count);
			preguntaActiva = preguntas[randomPregunta];
			preguntas.RemoveAt(randomPregunta);

			asignarValoresPregunta ();

		} catch {
			Debug.Log ("Error seleccionando pregunta");	
		}
	
	}

	void generarPreguntas(){
		try{
			preguntas = new List<PreguntaReto2> ();
			Transform goPreguntas = retoActivo.transform.FindChild("escenario").FindChild ("pregunta");

			for (int index = 0; index < goPreguntas.childCount; index ++) {
				Transform child = goPreguntas.GetChild(index);

				child.FindChild("titulo0").tag = "titulo";
				child.FindChild("Eslogan0").tag = "eslogan";
				child.FindChild("Imagen0").tag = "fondo";

				child.FindChild("titulo1").tag = "titulo";
				child.FindChild("Eslogan1").tag = "eslogan";
				child.FindChild("Imagen1").tag = "fondo";

				child.FindChild("titulo2").tag = "titulo";
				child.FindChild("Eslogan2").tag = "eslogan";
				child.FindChild("Imagen2").tag = "fondo";


				preguntas.Add( new PreguntaReto2 (
					child.FindChild("titulo0").gameObject,
					child.FindChild("Eslogan0").gameObject,
					child.FindChild("Imagen0").gameObject,
					new GameObject[]{child.FindChild("titulo1").gameObject, child.FindChild("titulo2").gameObject},
					new GameObject[]{child.FindChild("Eslogan1").gameObject, child.FindChild("Eslogan2").gameObject},
					new GameObject[]{child.FindChild("Imagen1").gameObject, child.FindChild("Imagen2").gameObject},
					int.Parse(child.name)
				));
			}

			asignarRespuestas();

		} catch {
			Debug.Log ("Error generando preguntas");
		}
	}

	void asignarRespuestas(){
		//Podólogo
		preguntas [0].SetTituloCorrecto (0);
		preguntas [0].SetEsloganCorrecto (1);
		preguntas [0].SetFondoCorrecto  (1);
		//Salón de belleza
		preguntas [1].SetTituloCorrecto (0);
		preguntas [1].SetEsloganCorrecto (2);
		preguntas [1].SetFondoCorrecto  (2);
		//Clínica oftalmológica
		preguntas [2].SetTituloCorrecto (0);
		preguntas [2].SetEsloganCorrecto (2);
		preguntas [2].SetFondoCorrecto  (1);
		//Medico general
		preguntas [3].SetTituloCorrecto (0);
		preguntas [3].SetEsloganCorrecto (1);
		preguntas [3].SetFondoCorrecto  (0);
		//Medico veterinario
		preguntas [4].SetTituloCorrecto (1);
		preguntas [4].SetEsloganCorrecto (2);
		preguntas [4].SetFondoCorrecto  (0);
		//Comida rápida
		preguntas [5].SetTituloCorrecto (1);
		preguntas [5].SetEsloganCorrecto (1);
		preguntas [5].SetFondoCorrecto  (0);
		//Gimnasio
		preguntas [6].SetTituloCorrecto (1);
		preguntas [6].SetEsloganCorrecto (1);
		preguntas [6].SetFondoCorrecto  (0);
		//Servicio mecánico
		preguntas [7].SetTituloCorrecto (0);
		preguntas [7].SetEsloganCorrecto (0);
		preguntas [7].SetFondoCorrecto  (2);
		//Frutas y verduras
		preguntas [8].SetTituloCorrecto (0);
		preguntas [8].SetEsloganCorrecto (2);
		preguntas [8].SetFondoCorrecto  (2);
		//Productos de limpieza
		preguntas [9].SetTituloCorrecto (0);
		preguntas [9].SetEsloganCorrecto (1);
		preguntas [9].SetFondoCorrecto  (0);
		//Centro de salud
		preguntas [10].SetTituloCorrecto (1);
		preguntas [10].SetEsloganCorrecto (2);
		preguntas [10].SetFondoCorrecto  (1);
	}

	public bool validarCorrecto(GameObject dropedObject, string tipo){

		int respuesta = dropedObject.GetComponent<DragReto2> ().respuesta;
		bool valido = false;
		switch(tipo)
		{
		case "titulo":
			if (respuesta == preguntaActiva.GetTituloCorrecto ()) {
				preguntaActiva.SetTituloCorrecto (0);
				valido = true;
			}
			break;
		case "eslogan":
			if (respuesta == preguntaActiva.GetEsloganCorrecto ()) {
				preguntaActiva.SetEsloganCorrecto (0);
				valido = true;
			}
			break;
		case "fondo":
			if (respuesta == preguntaActiva.GetFondoCorrecto ()) {
				preguntaActiva.SetFondoCorrecto (0);
				valido = true;
			}
			break;
		}

		if (valido) {
			genericUIScript.soundManager.playSound ("Sound_correcto_10",1);
			validarPreguntaTerminada ();
		} else {
			genericUIScript.soundManager.playSound ("Sound_incorrecto_19",1);
		}
		return valido;
	}

	void validarPreguntaTerminada()
	{
		if (preguntaActiva.GetTituloCorrecto () == 0 && preguntaActiva.GetEsloganCorrecto () == 0 && preguntaActiva.GetFondoCorrecto () == 0) {
			StartCoroutine (secuenciaEventos("openPopUp_preguntaCorrecta",1.0f));
		}
	}
}


public class PreguntaReto2
{
	GameObject _goTitulo;
	GameObject _goEslogan;
	GameObject _goFondo;
	GameObject[] _goTitulos;
	GameObject[] _goEslogans;
	GameObject[] _goFondos;
	int _tituloCorrecto;
	int _esloganCorrecto;
	int _fondoCorrecto;
	int _numOpcion;

	/// <summary>
	/// Preguntas del reto 2
	/// </summary>
	/// <param name="goTitulo">GameObject del titulo original</param>
	/// <param name="goEslogan">GameObject del slogan original</param>
	/// <param name="goFondo">GameObject del fondo original</param>
	/// <param name="goTitulos">GameObjects de los posibles titulos/param>
	/// <param name="goEslogans">GameObjects de los posibles slogans</param>
	/// <param name="goFondos">GameObjects de los posibles fondos</param>
	/// <param name="tituloCorrecto">Indice del titulo correcto</param>
	/// <param name="sloganCorrecto">Tema del anuncio</param>
	/// <param name="fondoCorrecto">Tema del anuncio</param>
	/// <param name="numOpcion">Numero de opcion en la lista de objetos</param>
	/// 
	public PreguntaReto2(GameObject goTitulo, GameObject goEslogan, GameObject goFondo,
		GameObject[] goTitulos, GameObject[] goEslogans, GameObject[] goFondos,
		int tituloCorrecto, int esloganCorrecto,  int fondoCorrecto, int numOpcion)
	{
		_goTitulo = goTitulo;
		_goEslogan = goEslogan;
		_goFondo = goFondo;
		_goTitulos = goTitulos;
		_goEslogans = goEslogans;
		_goFondos = goFondos;
		_tituloCorrecto = tituloCorrecto;
		_esloganCorrecto = esloganCorrecto;
		_fondoCorrecto = fondoCorrecto;
		_numOpcion = numOpcion;
	}

	/// <summary>
	/// Preguntas del reto 2, sin el número de respuesta
	/// </summary>
	/// <param name="goTitulo">GameObject del titulo original</param>
	/// <param name="goEslogan">GameObject del eslogan original</param>
	/// <param name="goFondo">GameObject del fondo original</param>
	/// <param name="goTitulos">GameObjects de los posibles titulos/param>
	/// <param name="goEslogans">GameObjects de los posibles eslogans</param>
	/// <param name="goFondos">GameObjects de los posibles fondos</param>
	/// <param name="numOpcion">Numero de opcion en la lista de objetos</param>
	/// 
	public PreguntaReto2(GameObject goTitulo, GameObject goEslogan, GameObject goFondo,
		GameObject[] goTitulos, GameObject[] goEslogans, GameObject[] goFondos,
		int numOpcion)
	{
		_goTitulo = goTitulo;
		_goEslogan = goEslogan;
		_goFondo = goFondo;
		_goTitulos = goTitulos;
		_goEslogans = goEslogans;
		_goFondos = goFondos;
		_numOpcion = numOpcion;

		_tituloCorrecto = 0;
		_esloganCorrecto = 0;
		_fondoCorrecto = 0;
	}

	public GameObject GetGOTitulo(){
		return _goTitulo;
	}

	public GameObject GetGOEslogan(){
		return _goEslogan;
	}

	public GameObject GetGOFondo(){
		return _goFondo;
	}

	public GameObject[] GetGOTitulos(){
		return _goTitulos;
	}

	public GameObject[] GetGOEslogans(){
		return _goEslogans;
	}

	public GameObject[] GetGOFondos(){
		return _goFondos;
	}

	public int GetTituloCorrecto(){
		return _tituloCorrecto;
	}

	public void SetTituloCorrecto(int tituloCorrecto){
		_tituloCorrecto = tituloCorrecto;
	}

	public int GetEsloganCorrecto(){
		return _esloganCorrecto;
	}

	public void SetEsloganCorrecto(int esloganCorrecto){
		_esloganCorrecto = esloganCorrecto;
	}

	public int GetFondoCorrecto(){
		return _fondoCorrecto;
	}

	public void SetFondoCorrecto(int fondoCorrecto){
		_fondoCorrecto = fondoCorrecto;
	}

	public int GetNumOpcion(){
		return _numOpcion;
	}

}

public class DragReto2 : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

	Vector3 posicionOriginal;
	public int respuesta;

	public void OnBeginDrag(PointerEventData eventData)
	{
		posicionOriginal = transform.localPosition;

		//Se agrupa con el cursor
		if(gameObject.GetComponent<CanvasGroup>() == null){
			gameObject.AddComponent<CanvasGroup>();
		}
			
		//Se indica que no estorbe los ray casts
		gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;

	}

	public void OnDrag(PointerEventData eventData)
	{
		Vector3 globalMousePos;
		RectTransformUtility.ScreenPointToWorldPointInRectangle (
			eventData.pointerEnter.transform as RectTransform, eventData.position, 
			eventData.pressEventCamera, out globalMousePos
		);
		transform.position = globalMousePos;
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		transform.localPosition = posicionOriginal;
		gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;
	}
}

public class DropReto2 : MonoBehaviour, IDropHandler
{
	Reto2_ODA01 retoScript;

	public void asignarValores(Reto2_ODA01 script)
	{
		retoScript = script;
	}

	public void OnDrop(PointerEventData data)
	{
		if (transform.tag == data.pointerDrag.tag && retoScript.validarCorrecto (data.pointerDrag, transform.tag)) {
			//Obtiene imagen del objeto que se tiro en este objeto
			Sprite dropSprite = GetDropSprite (data);
			//Si es la respuesta correcta se le cambia la imagen
			if (dropSprite != null) {
				this.GetComponent<Image> ().sprite = dropSprite;
				this.GetComponent<Image> ().color = Color.white;
				data.pointerDrag.SetActive (false);
				if(transform.tag == "eslogan" || transform.tag == "titulo")
					this.GetComponent<Image> ().SetNativeSize();
				
			}
		}
	}
		
	private Sprite GetDropSprite(PointerEventData data)
	{
		//Se obtiene el objeto que se esta arrastrando, si no existe se sale
		var originalObj = data.pointerDrag;
		if (originalObj == null)
			return null;

		
		//Se obtiene la imagen del objeto que se arrastra, si es nulo se regresa nulo
		var srcImage = originalObj.GetComponent<Image>();
		if (srcImage == null)
			return null;

		return srcImage.sprite;
	}
}