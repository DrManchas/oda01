﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Reto1_ODA01 : MonoBehaviour {

	//Scripts ayuda
	GenericUI_ODA01 genericUIScript;
	Menu_ODA01 menuScript;

	public GameObject retoActivo;
	PreguntaReto1 preguntaActual;

	List<List<PreguntaReto1>> preguntas = new List<List<PreguntaReto1>>();

	List<int> categoriasPasadas = new List<int>();

	int herramientasGanadas = 0;
	bool recorrerCamara = false;

	public enum Categorias
	{
		publicidad,
		educacion,
		politica,
		social
	}

	// Use this for initialization
	void Start () {
	
		//Scripts de Ayuda
		genericUIScript = this.transform.GetComponent<GenericUI_ODA01>();
		menuScript = this.transform.GetComponent<Menu_ODA01>();
	}
	
	// Update is called once per frame
	void Update () {
		if (retoActivo != null) {
			recorridoCamara ();
		}
	}

	// ------------------------Genericos Retos----------------------

	//Iniciar clone
	public void inicarReto (GameObject reto){

		//Relacionar objetos
		retoActivo = reto;

		//Generar preguntas
		generarPreguntas ();

		//seleccionar anuncio
		categoriasPasadas = new List<int>();
		seleccionarAnuncio();

		//Funciones botones
		Button btnPregunta = retoActivo.transform.FindChild ("escenario").FindChild("btnPregunta").GetComponent<Button>();
		btnPregunta.onClick.AddListener(delegate() {
			abrirPregunta();
		});

		Button btnAvanzar = retoActivo.transform.FindChild("avanzar").GetComponent<Button>();
		btnAvanzar.onClick.AddListener(delegate() {
			retoActivo.transform.FindChild ("avanzar").GetComponent<Animator> ().Play ("avanzarDefault");
			recorrerCamara = !recorrerCamara;
		});

		herramientasGanadas = 0;
		recorrerCamara = false;

		genericUIScript.soundManager.playSound ("Sound_CarrosCalle", 0);

		//Mostrar popUp
		StartCoroutine(secuenciaEventos("openPopUp_introduccion",0.75f));
	}

	//Funciones secuencia
	public IEnumerator secuenciaEventos (string secuencia, float tiempoEspera)
	{
		yield return new WaitForSeconds (tiempoEspera);
		string[] instrucciones = secuencia.Split ('_');
		switch (instrucciones[0]) {
		case "openPopUp":
			openPopUp (instrucciones[1]);
			break;
		case "closePopUp":
			closePopUp (instrucciones[1]);
			break;
		case "reiniciarReto":
			//genericUIScript.reiniciarEscena(1);;
			break;
		}
	}

	//Eventos al abrir popUp

	public void openPopUp (string popUpName)
	{
		//genericUIScript.soundManager.playSound ("Sound_OpenPopUp 10",1);
		genericUIScript.gameUI.SetActive(false);
		switch(popUpName){
		case "introduccion":
			genericUIScript.mostrarInfoPopUp (
				"¡Bienvenido a Ítaca! Esta ciudad no descansa nunca… Siempre está en movimiento. ¿Quieres saber por qué?",
				"closePopUp_introduccion",
				1,
				1.0f
			);
			break;
		case "instrucciones":
			genericUIScript.mostrarInfoPopUp (
				"En cada zona de la ciudad te encontrarás con diferentes retos, resuélvelos para obtener las herramientas " +
				"que te permitirán completar el reto final.",
				"closePopUp_instrucciones",
				1,
				1.00f
			);
			break;
		case "preguntaCorrecta":
			resetPosicionFondo ();
			preguntaCorrecta ();
			break;
		case "preguntaIncorrecta":
			resetPosicionFondo ();
			preguntaIncorrecta ();
			break;
		case "ganarReto":
			genericUIScript.soundManager.playSound ("TerminarReto",1);
			genericUIScript.mostrarInfoPopUp (
				"¡Muy bien! Hay más anuncios esperándonos. ¡Sigamos con el segundo reto!",
				"closePopUp_ganarReto",
				1,
				0.00f
			);
			break;
		case "perderReto":
			genericUIScript.mostrarInfoPopUp (
				"Te equivocaste varias veces, vuelve a intentarlo.",
				"closePopUp_perderReto",
				1,
				0.00f
			);
			break;
		}

	}

	//Eventos al cerrar popUp
	void closePopUp (string popUpName)
	{
		switch(popUpName){
		case "introduccion":
			StartCoroutine (secuenciaEventos("openPopUp_instrucciones", 0.5f));
			break;
		case "instrucciones":
			genericUIScript.gameUI.SetActive (true);
			Transform avanzar = retoActivo.transform.FindChild ("avanzar");
			avanzar.gameObject.SetActive (true);
			avanzar.GetComponent<Animator> ().Play ("avanzar");
			break;
		case "preguntaCorrecta":
			break;
		case "preguntaIncorrecta":
			break;
		case "ganarReto":
			Destroy (retoActivo);
			menuScript.abrirReto (1);
			break;
		case "perderReto":
			Destroy (retoActivo);
			menuScript.abrirReto (0);
			break;
		}
	}

	// ------------------------Individuales Reto----------------------

	void resetPosicionFondo()
	{

		Transform escenario = retoActivo.transform.FindChild ("escenario");
		Vector3 position = escenario.localPosition;
		position.x = 900;
		escenario.localPosition = position;

		Transform avanzar = retoActivo.transform.FindChild ("avanzar");
		avanzar.gameObject.SetActive (true);
		avanzar.GetComponent<Animator> ().Play ("avanzar");

	}

	void recorridoCamara()
	{
		if (recorrerCamara) 
		{
			Transform escenario = retoActivo.transform.FindChild ("escenario");

			if (escenario.localPosition.x < 0) {
				retoActivo.transform.FindChild ("avanzar").gameObject.SetActive (false);
				recorrerCamara = false;
			} else {
				Vector3 position = escenario.localPosition;
				position.x = position.x - 5;
				escenario.localPosition = position;
			}
		}

	}

	void abrirPregunta(){
		retoActivo.transform.FindChild ("escenario").FindChild ("pregunta").gameObject.SetActive(true);
		genericUIScript.soundManager.playSound("Fondo_Anuncios",0);
		genericUIScript.gameUI.SetActive(false);

		Transform escenario = retoActivo.transform.FindChild ("escenario");
		Vector3 position = escenario.localPosition;
		position.x = 0;
		escenario.localPosition = position;

		retoActivo.transform.FindChild ("avanzar").gameObject.SetActive (false);
	}

	void preguntaCorrecta()
	{
		genericUIScript.gameUI.SetActive (true);
		genericUIScript.soundManager.playSound ("Sound_correcto_10",1);
		genericUIScript.soundManager.playSound ("Sound_CarrosCalle", 0);
		categoriasPasadas.Add ((int)preguntaActual.GetTemaAnuncio ());
		genericUIScript.ganarHerramienta ((GenericUI_ODA01.Herramientas)herramientasGanadas);
		herramientasGanadas++;
		if (herramientasGanadas >= 4) {
			StartCoroutine (secuenciaEventos ("openPopUp_ganarReto", 2.3f));
		} else {
			seleccionarAnuncio ();
		}
	}

	void preguntaIncorrecta()
	{
		genericUIScript.gameUI.SetActive(true);
		genericUIScript.soundManager.playSound ("Sound_CarrosCalle",0);
		genericUIScript.soundManager.playSound ("Sound_incorrecto_19",1);
		genericUIScript.mostrarInfoPopUp (
			""+preguntaActual.GetTxtProposito(),
			"closePopUp_preguntaIncorrecta",
			1,
			0.00f
		);
		seleccionarAnuncio ();
	}

	void seleccionarAnuncio(){
	
		int randomCategoria = Random.Range (0,4);
		int intentos = 0;
		while(categoriasPasadas.Contains(randomCategoria)){
			if (intentos > 5)
				break;
			randomCategoria = Random.Range (0,4);
			intentos++;
		}
		if (preguntas [randomCategoria].Count <= 0) {
			StartCoroutine (secuenciaEventos("openPopUp_perderReto", 0.75f));
		} else {
			int randomPregunta = Random.Range (0,preguntas[randomCategoria].Count);
			preguntaActual = preguntas[randomCategoria][randomPregunta];
			//preguntas[randomCategoria].RemoveAt(randomPregunta);

			asignarValoresPregunta ();
		}

	}

	void asignarValoresPregunta (){

		//Anucio calle
		desactivarAnuncios();
		GameObject anuncio = retoActivo.transform.FindChild ("escenario").FindChild ("anuncio").
			FindChild(""+preguntaActual.GetTemaAnuncio().ToString()).GetChild(preguntaActual.GetNumAnuncio()-1).gameObject;
		anuncio.SetActive (true);


		//Area de pregunta
		GameObject pregunta = retoActivo.transform.FindChild ("escenario").FindChild ("pregunta").gameObject;

		//Area imagen
		pregunta.transform.FindChild ("imgAnuncio").GetComponent<Image>().sprite = 
			anuncio.transform.GetComponent<Image>().sprite;

		//Texto Pregunta
		pregunta.transform.FindChild ("txtPregunta").GetComponent<Text> ().text = preguntaActual.GetTxtPregunta();

		//temporal para respuestas
		List<string> tmpRespuestas = new List<string> ();
		tmpRespuestas.Add (preguntaActual.GetTxtRespuestas () [0]);
		tmpRespuestas.Add (preguntaActual.GetTxtRespuestas () [1]);
		tmpRespuestas.Add (preguntaActual.GetTxtRespuestas () [2]);

		//Asignacion de valores a botones
		int randomRespuesta = Random.Range (0, tmpRespuestas.Count);
		Button btnRespuesta1 = pregunta.transform.FindChild ("btnRespuesta1").GetComponent<Button> ();
		asignarValoresBtnRespuesta (btnRespuesta1, "A)", tmpRespuestas[randomRespuesta]);
		tmpRespuestas.RemoveAt (randomRespuesta);

		randomRespuesta = Random.Range (0, tmpRespuestas.Count);
		Button btnRespuesta2 = pregunta.transform.FindChild ("btnRespuesta2").GetComponent<Button> ();
		asignarValoresBtnRespuesta (btnRespuesta2, "B)", tmpRespuestas[randomRespuesta]);
		tmpRespuestas.RemoveAt (randomRespuesta);

		randomRespuesta = Random.Range (0, tmpRespuestas.Count);
		Button btnRespuesta3 = pregunta.transform.FindChild ("btnRespuesta3").GetComponent<Button> ();
		asignarValoresBtnRespuesta (btnRespuesta3, "C)", tmpRespuestas[randomRespuesta]);
		tmpRespuestas.RemoveAt (randomRespuesta);
	}

	void asignarValoresBtnRespuesta(Button btnRespuesta, string inciso, string respuesta){
		btnRespuesta.onClick.RemoveAllListeners ();
		btnRespuesta.transform.GetChild (0).GetComponent<Text>().text = ""+inciso+" "+respuesta;
		if (respuesta == preguntaActual.GetTxtRespuestas () [preguntaActual.GetRespuestaCorrecta ()-1]) {
			btnRespuesta.onClick.AddListener (delegate() {
				retoActivo.transform.FindChild ("escenario").FindChild ("pregunta").gameObject.SetActive(false);
				StartCoroutine (secuenciaEventos("openPopUp_preguntaCorrecta",0.5f));
			});
		} else {
			btnRespuesta.onClick.AddListener (delegate() {
				retoActivo.transform.FindChild ("escenario").FindChild ("pregunta").gameObject.SetActive(false);
				StartCoroutine(secuenciaEventos ("openPopUp_preguntaIncorrecta", 0.75f));
			});
		}
	}

	void desactivarAnuncios(){
	
		Transform anuncios = retoActivo.transform.FindChild ("escenario").FindChild ("anuncio");
		foreach (Transform categorias in anuncios) {
			foreach (Transform anuncio in categorias) {
				anuncio.gameObject.SetActive (false);
			}
		}
	}

	void generarPreguntas(){
	
		preguntas = new List<List<PreguntaReto1>> ();

		preguntas.Add(generarPreguntasPublicidad ());
		preguntas.Add(generarPreguntasEducacion ());
		preguntas.Add(generarPreguntasPolitica ());
		preguntas.Add(generarPreguntasSocial ());

	}

	List<PreguntaReto1> generarPreguntasPublicidad(){

		//Limpiar lista
		List<PreguntaReto1> preguntasPublicidad = new List<PreguntaReto1> ();

		//Generar lista

		preguntasPublicidad.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que las personas al comprar ahí tienen muchos beneficios.",
				"Que las personas al comprar ahí tienen más estatus.",
				"Que es la mejor comida de la zona."},
			"El propósito de este anuncio es que la gente sepa que por un mismo precio obtiene muchos beneficios.",
			1,
			Categorias.publicidad,
			2
		));

		preguntasPublicidad.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que las personas al comerlos son más divertidas.",
				"Que ahora los dulces son mejores y estarán siempre en el mercado.",
				"Que son los mejores dulces del nuevo siglo."},
			"El propósito de este anuncio es que el público sepa que los dulces están de nuevo en el mercado y son " +
			"mejores que antes.",
			2,
			Categorias.publicidad,
			3
		));

		preguntasPublicidad.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que es el mejor café del mundo.",
				"Que puedes entrar con tu gato a tomar café.",
				"Que el aroma es su principal atractivo."},
			"Con este anuncio se quiere informar al público que el aroma de este café es tan rico que quien lo huela, " +
			"quedará enamorado.",
			3,
			Categorias.publicidad,
			4
		));

		preguntasPublicidad.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que quien coma en el restaurante tiene gustos refinados.",
				"Que el vino es su mejor opción.",
				"Que la comida es costosa pero rica."},
			"El propósito del anuncio es invitar a la gente a comer en el restaurante pues tendrá una experiencia que " +
			"sólo los gustos refinados tienen.",
			1,
			Categorias.publicidad,
			5
		));

		preguntasPublicidad.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que quien coma en el restaurante tiene gustos refinados. ",
				"Que el que coma en ese restaurante se merece una excelente comida.",
				"Que la comida es más rica que la del resto de restaurantes."},
			"El propósito del anuncio es que la gente se sienta especial al comer en el restaurante.",
			1,
			Categorias.publicidad,
			6
		));
		return preguntasPublicidad;
	}

	//Preguntas de educacion
	List<PreguntaReto1> generarPreguntasEducacion(){

		//Limpiar lista
		List<PreguntaReto1> preguntasEducacion = new List<PreguntaReto1> ();

		//Generar lista
		preguntasEducacion.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que quien estudie en la universidad será más inteligente.",
				"Quien estudie en la universidad se merece un descuento.",
				"Que el descuento se otorga por promedio."},
			"El propósito del anuncio es que la gente se inscriba y reciba un descuento.",
			2,
			Categorias.educacion,
			1
		));

		preguntasEducacion.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que las cosas que suceden en el futuro son decisión de cada persona.",
				"Que el futuro es predecible.",
				"Que no se puede hacer nada para cambiar el futuro."},
			"El propósito del anuncio es que las personas sepan que sus decisiones tienen impacto en su vida.",
			1,
			Categorias.educacion,
			2
		));

		preguntasEducacion.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"No se hacen distinciones entre los estudiantes.",
				"Que todas las personas que estudien serán distinguidas.",
				"Que no todas las personas son inteligentes."},
			"El propósito del anuncio es que las personas sepan que la inclusión es un aspecto fundamental en la " +
			"institución educativa.",
			1,
			Categorias.educacion,
			3
		));
		return preguntasEducacion;
	}

	List<PreguntaReto1> generarPreguntasPolitica(){
	
		//Limpiar lista
		List<PreguntaReto1> preguntasPolitica = new List<PreguntaReto1> ();

		//Generar lista
		preguntasPolitica.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que votar para ellos es la mejor opción.",
				"Que votar por ellos tiene múltiples beneficios.",
				"Que votar es una forma de contribuir con la democracia."},
			"El propósito del anuncio es que existe una opción de voto que supera al resto.",
			1,
			Categorias.politica,
			1
		));

		preguntasPolitica.Add( new PreguntaReto1(
			"¿Qué reacción se quiere provocar en las personas que ven el anuncio?",
			new string[]{"Que se unan a su partido",
				"Que formen parte del cambio al votar con democracia.",
				"Que voten por un partido político en particular."},
			"El anuncio quiere persuadir al público, pidiendo el voto para ganar. ",
			3,
			Categorias.politica,
			2
		));

		preguntasPolitica.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que el voto es una herramienta de la democracia.",
				"Que el voto es imprescindible para ganar. ",
				"Que el candidato es un ciudadano más y se puede trabajar en equipo."},
			"El propósito del anuncio es que los ciudadanos y los candidatos pueden trabajar en equipo para " +
			"mejorar las condiciones. ",
			3,
			Categorias.politica,
			3
		));
		return preguntasPolitica;
	}

	List<PreguntaReto1> generarPreguntasSocial(){
	
		//Limpiar lista
		List<PreguntaReto1> preguntasSocial = new List<PreguntaReto1> ();

		//Generar lista
		preguntasSocial.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que el Sol es un aspecto típico de las vacaciones.",
				"Que el cuidarse del Sol puede prevenir enfermedades.",
				"Que las familias se asolean cuando salen de vacaciones."},
			"El propósito del anuncio es que las personas sepan que deben cuidarse del Sol.",
			2,
			Categorias.social,
			1
		));

		preguntasSocial.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que todas las enfermedades se previenen.",
				"Que  todas las enfermedades son curables.",
				"Que una forma de prevenir enfermedades es acudiendo periódicamente al médico."},
			"El propósito del anuncio es que la gente esté informada de que la prevención es un aspecto fundamental" +
			" para el cuidado de la salud.",
			3,
			Categorias.social,
			2
		));

		preguntasSocial.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que la participación vecinal es imprescindible para la toma de decisiones",
				"Que un problema de salud no tiene nada que ver con la comunidad.",
				"Que  si existe un problema de salud sólo los científicos sabrán qué hacer."},
			"El propósito del anuncio es invitar a la gente a participar en las decisiones colectivas.",
			1,
			Categorias.social,
			3
		));

		preguntasSocial.Add( new PreguntaReto1(
			"¿Qué mensaje busca transmitir este anuncio?",
			new string[]{"Que estar informados de las enfermedades nos hace tomar mejores decisiones",
				"Que la influenza es mortal.",
				"Que  la influenza le da sólo a los que se dan la mano."},
			" El propósito del anuncio es dar conocer los métodos de prevención de la influenza para tomar buenas decisiones.",
			1,
			Categorias.social,
			4
		));
		return preguntasSocial;
	}
		
}

public class PreguntaReto1
{
	private string _txtPregunta;
	private string[] _txtRespuestas;
	private int _respuestaCorrecta;
	private string _txtProposito;
	private Reto1_ODA01.Categorias _temaAnuncio;
	private int _numAnuncio;

	/// <summary>
	/// Preguntas del reto 1
	/// </summary>
	/// <param name="txtPregunta">Texto de la pregunta</param>
	/// <param name="txtRespuestas">Texto de las respuestas</param>
	/// <param name="respuestaCorrecta">Respuesta correcta. Nota: Entero mayor a 0</param>
	/// <param name="txtProposito">Texto del proposito del anuncio/param>
	/// <param name="temaAnuncio">Tema del anuncio</param>
	/// <param name="numAnuncio">Numero de objeto hijo del objeto del tema</param>
	public PreguntaReto1(string txtPregunta, string[] txtRespuestas, string txtProposito,
		int respuestaCorrecta, Reto1_ODA01.Categorias temaAnuncio, int numAnuncio)
	{
		_txtPregunta = txtPregunta;
		_txtRespuestas = txtRespuestas;
		_respuestaCorrecta = respuestaCorrecta;
		_txtProposito = txtProposito;
		_temaAnuncio = temaAnuncio;
		_numAnuncio = numAnuncio;
	}
		
	public string GetTxtPregunta(){
		return _txtPregunta;
	}

	public string[] GetTxtRespuestas(){
		return _txtRespuestas;
	}

	public int GetRespuestaCorrecta(){
		return _respuestaCorrecta;
	}

	public string GetTxtProposito(){
		return _txtProposito;
	}

	public Reto1_ODA01.Categorias GetTemaAnuncio(){
		return _temaAnuncio;
	}

	public int GetNumAnuncio(){
		return _numAnuncio;
	}

}
