﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Menu_ODA01 : MonoBehaviour {

	//Scripts Ayuda
	GenericUI_ODA01 genericUIScript;

	//This obj
	[HideInInspector]
	public GameObject menu;

	[HideInInspector]
	public List<GameObject> retos;

	[HideInInspector]
	public GameObject retoActivo = null;


	//Botones
	[HideInInspector]
	public List<Button> botones;

	void Awake(){

		//Script de ayuda
		genericUIScript = transform.GetComponent<GenericUI_ODA01>();

		//Pantallas
		retos = new List<GameObject>();
		menu = this.transform.FindChild("Menu").gameObject;
		retos.Add (this.transform.FindChild ("reto1").gameObject); 
		retos.Add (this.transform.FindChild ("reto2").gameObject);
		retos.Add (this.transform.FindChild ("reto3").gameObject);
		retos.Add (this.transform.FindChild ("reto4").gameObject);
		retos.Add (this.transform.FindChild ("retoFinal").gameObject);
	}

	// Use this for initialization
	void Start () {

		//Asignación de camara
		for (int index = 0; index < retos.Count; index++) {
			retos[index].GetComponent<Canvas> ().worldCamera = Camera.main;
		}

		//Botones
		botones = new List<Button>();
		botones.Add(menu.transform.FindChild("btnReto1").GetComponent<Button>());
		botones.Add(menu.transform.FindChild("btnReto2").GetComponent<Button>());
		botones.Add(menu.transform.FindChild("btnReto3").GetComponent<Button>());
		botones.Add(menu.transform.FindChild("btnReto4").GetComponent<Button>());
		botones.Add(menu.transform.FindChild("btnRetoFinal").GetComponent<Button>());

		//Funciones boton
		botones[0].onClick.AddListener (delegate() {
			abrirReto(0);
		});

		botones[1].onClick.AddListener (delegate() {
			abrirReto(1);
		});

		botones[2].onClick.AddListener (delegate() {
			abrirReto(2);
		});

		botones[3].onClick.AddListener (delegate() {
			abrirReto(3);
		});

		botones[4].onClick.AddListener (delegate() {
			abrirReto(4);
		});

	}

	//Funcion botones elegir reto
	public void abrirReto(int reto){

		genericUIScript.soundManager.stopSound (0);
		menu.SetActive (false);

		retoActivo = Instantiate (retos [reto]);
		retoActivo.name = "instanciaReto"+(1+reto);
		retoActivo.SetActive (true);
		retoActivo.transform.SetParent(transform);

		switch (reto) 
		{
		case 0:
			this.GetComponent<Reto1_ODA01> ().inicarReto (retoActivo);
			break;
		case 1:
			this.GetComponent<Reto2_ODA01> ().inicarReto (retoActivo);
			break;
		case 2:
			this.GetComponent<Reto3_ODA01> ().inicarReto (retoActivo);
			break;
		case 3:
			genericUIScript.gameUI.SetActive (false);
			this.GetComponent<Reto4_ODA01> ().inicarReto (retoActivo);
			break;
		case 4:
			genericUIScript.gameUI.SetActive (false);
			this.GetComponent<RetoFinal_ODA01> ().inicarReto (retoActivo);
			break;
		}
			
	}

	public void abrirMenu()
	{
		menu.SetActive (true);
		genericUIScript.genericUI.SetActive (true);
		genericUIScript.gameUI.SetActive (false);
		Destroy (retoActivo);
		retoActivo = null;
	}
}
