﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class Reto3_ODA01 : MonoBehaviour {

	//Scripts ayuda
	public GenericUI_ODA01 genericUIScript;
	Menu_ODA01 menuScript;
	Reto3_ODA01 reto3Script;

	public GameObject retoActivo;

	List<PreguntaReto3> frases = new List<PreguntaReto3> ();
	List<PreguntaReto3> frasesSeleccionadas = new List<PreguntaReto3> ();

	int errores = 0;
	bool retoTerminado = false;

	// Use this for initialization
	void Start () {

		//Scripts de Ayuda
		genericUIScript = this.transform.GetComponent<GenericUI_ODA01>();
		menuScript = this.transform.GetComponent<Menu_ODA01>();
		reto3Script = this.transform.GetComponent<Reto3_ODA01>();
	}

	// Update is called once per frame
	void Update () {
		if(retoActivo != null && !retoTerminado)
			retoTerminado = validarRetoTerminado ();
	}

	// ------------------------Genericos Retos----------------------

	//Iniciar clone
	public void inicarReto (GameObject reto){

		retoActivo = reto;

		genericUIScript.soundManager.stopSound (0);
		StartCoroutine ( sonidosAnimacion ());


		//Generar frases
		generarFrases ();

		//seleccionar frases
		seleccionarFrases();

		errores = 0;
		retoTerminado = false;

		retoActivo.transform.FindChild ("Escena1").gameObject.SetActive(true);
		retoActivo.transform.FindChild ("Escena2").gameObject.SetActive(false);
	
		//Indicaciones
		StartCoroutine(secuenciaEventos("openPopUp_introduccion", 3.0f));

	}

	//Funciones secuencia
	public IEnumerator secuenciaEventos (string secuencia, float tiempoEspera)
	{
		yield return new WaitForSeconds (tiempoEspera);
		string[] instrucciones = secuencia.Split ('_');
		switch (instrucciones[0]) {
		case "openPopUp":
			openPopUp (instrucciones[1]);
			break;
		case "closePopUp":
			closePopUp (instrucciones[1]);
			break;
		case "reiniciarReto":
			//genericUIScript.reiniciarEscena(1);;
			break;
		}
	}

	//Eventos al abrir popUp

	public void openPopUp (string popUpName)
	{
		genericUIScript.gameUI.SetActive(false);

		switch(popUpName){
		case "introduccion":
			retoActivo.transform.FindChild ("Escena1").gameObject.SetActive(false);
			retoActivo.transform.FindChild ("Escena2").gameObject.SetActive(true);
			genericUIScript.soundManager.playSound ("Sound_Corazon", 0);
			genericUIScript.mostrarInfoPopUp (
				"¡Estás en la quinta dimensión! Analiza las frases sugestivas presionando sobre ellas para escanearlas" +
				" y arrastra hacia el agujero negro las que no son saludables.",
				"closePopUp_introduccion",
				3,
				1.00f
			);
			break;
		case "reiniciarReto":
			genericUIScript.mostrarInfoPopUp (
				"Analiza cuidadosamente las frases para saber cuáles no mandan un mensaje saludable. ",
				"closePopUp_reiniciarReto",
				3,
				1.00f
			);
			break;
		case "ganarReto":
			genericUIScript.soundManager.playSound ("TerminarReto",1);
			genericUIScript.mostrarInfoPopUp (
				"Ahora sabes cómo analizar una frase sugestiva para tomar mejores decisiones. ¿Podrás ayudar  al pueblo " +
				"de Ítaca? ¡Sigamos con el cuarto reto!",
				"closePopUp_ganarReto",
				3,
				0.00f
			);
			break;
		}

	}

	//Eventos al cerrar popUp
	void closePopUp (string popUpName)
	{
		switch(popUpName){
		case "introduccion":
			activarVortex();
			break;
		case "ganarReto":
			Destroy (retoActivo);
			retoTerminado = false;
			retoActivo = null;
			menuScript.abrirReto (3);
			break;
		}
	}

	// ------------------------Individuales Reto----------------------

	// ------------------------Vista---------------------------------

	IEnumerator sonidosAnimacion()
	{
		genericUIScript.soundManager.playSound ("Sound_breaking",1);
		yield return new WaitForSeconds (1.2f);
		genericUIScript.soundManager.playSound ("Sound_ScreamLv03",1);
	}

	void activarVortex()
	{
		GameObject vortex = retoActivo.transform.FindChild ("Escena2").FindChild ("vortex").gameObject;
		vortex.SetActive (true);
		vortex.AddComponent <DropReto3>();
		vortex.GetComponent<DropReto3> ().asignarValores (reto3Script);
	}

	public void desactivarFrasesEscaner()
	{
		Transform goEscaner = retoActivo.transform.FindChild ("Escena2").FindChild("escaner");
		foreach (Transform frase in goEscaner) {
			frase.gameObject.SetActive (false);
		}

		retoActivo.transform.FindChild ("Escena2").FindChild ("fondos").
		FindChild ("fondo2").gameObject.SetActive (false);

	}

	public void activarFrasesEscaner(string nombreFrase)
	{
		retoActivo.transform.FindChild ("Escena2").FindChild ("escaner").
		FindChild (nombreFrase).gameObject.SetActive (true);

		retoActivo.transform.FindChild ("Escena2").FindChild ("fondos").
		FindChild ("fondo2").gameObject.SetActive (true);
	}

	void desactivarFrases()
	{
		Transform goFrases = retoActivo.transform.FindChild ("Escena2").FindChild("frases");
		foreach (Transform frase in goFrases) {
			frase.gameObject.SetActive (false);
			//Destroy (frase.gameObject.GetComponent("DragReto3"));
		}
	}

	// ------------------------Logica---------------------------------

	void seleccionarFrases()
	{
		try{
			frasesSeleccionadas = new List<PreguntaReto3>();

			for(int index=0;index<5;index++){
				int randomfrase = Random.Range(0,frases.Count);
				frasesSeleccionadas.Add(frases[randomfrase]);

				frases[randomfrase].GetFrase().SetActive(true);
				frases.RemoveAt(randomfrase);
			}
		} catch {
			Debug.Log ("Error seleccionando pregunta");	
		}

	}

	void generarFrases(){
		try{
			desactivarFrases();
			frases = new List<PreguntaReto3> ();
			Transform goFrases = retoActivo.transform.FindChild ("Escena2").FindChild("frases");
			Transform goEscaner = retoActivo.transform.FindChild ("Escena2").FindChild("escaner");

			for (int index = 0; index < goFrases.childCount; index ++) {
				Transform childFrase = goFrases.GetChild(index);
				Transform childEscaner = goEscaner.GetChild(index);

				frases.Add( new PreguntaReto3 (
					childFrase.gameObject,
					childEscaner.gameObject
				));
			}

			asignarRespuestas();
		} catch {
			Debug.Log ("Error obteniendo frases");
		}
	}

	void asignarRespuestas(){
		frases [0].SetBasura (false);
		frases [1].SetBasura (false);
		frases [2].SetBasura (true);
		frases [3].SetBasura (true);
		frases [4].SetBasura (true);
		frases [5].SetBasura (true);
		frases [6].SetBasura (false);
		frases [7].SetBasura (false);
		frases [8].SetBasura (false);
		frases [9].SetBasura (true);
		frases [10].SetBasura (true);
		frases [11].SetBasura (true);
		frases [12].SetBasura (true);

		foreach (PreguntaReto3 frase in frases) 
		{
			if (frase.GetFrase ().GetComponent<DragReto3> () == null) {
				frase.GetFrase ().AddComponent<DragReto3>();
				frase.GetFrase ().GetComponent<DragReto3>().asignarValores
				(reto3Script,frase.GetBasura(), 
					retoActivo.transform.FindChild("Escena2").FindChild("apuntador").gameObject);
			}
		}
	}

	public void validarFraseCorrecta(PointerEventData data)
	{
		GameObject dropedObject = data.pointerDrag; 

		dropedObject.GetComponent<DragReto3> ().OnEndDrag(data);

		bool basura = data.pointerDrag.GetComponent<DragReto3> ().GetBasura ();
		if (!basura) {
			errores++;
			genericUIScript.soundManager.playSound ("Sound_incorrecto_19", 1);
		} else {
			genericUIScript.soundManager.playSound ("Sound_Cracking", 1);

			for (int index = 0 ; index < frasesSeleccionadas.Count; index++) 
			{
				if (frasesSeleccionadas [index].GetFrase ().name == dropedObject.name) {
					frasesSeleccionadas.Remove (frasesSeleccionadas[index]);
					break;
				}
			}

			dropedObject.SetActive (false);
		}

		if(errores>=2)
		{
			StartCoroutine(secuenciaEventos("openPopUp_reiniciarReto",0.5f));
			generarFrases ();
			seleccionarFrases ();
			errores = 0;
		}
	}

	bool validarRetoTerminado()
	{
		foreach (PreguntaReto3 frase in frasesSeleccionadas) 
		{
			if (frase.GetBasura ())
				return false;
		}

		StartCoroutine(secuenciaEventos("openPopUp_ganarReto", 0.75f));
		return true;
	}
}


public class PreguntaReto3
{
	GameObject _frase;
	GameObject _mensaje;
	bool _basura;

	/// <summary>
	/// Preguntas del reto 3
	/// </summary>
	/// <param name="frase"></param>
	/// <param name="mensaje"></param>
	/// 
	public PreguntaReto3(GameObject frase, GameObject mensaje)
	{
		_frase = frase;
		_mensaje = mensaje;
		_basura = true;
	}

	public GameObject GetFrase(){
		return _frase;
	}

	public GameObject GetMensaje(){
		return _mensaje;
	}

	public bool GetBasura(){
		return _basura;
	}

	public void SetBasura(bool value){
		_basura = value;
	}
		
}

public class DragReto3 : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{

	Reto3_ODA01 retoScript;
	bool basura;
	GameObject apuntador;
	bool dragging = false;
	int ejeX = 1;
	int ejeY = 1;
	int ejeZ = 1;

	void Update()
	{
		if (!dragging) 
		{
			moverFrase ();
		}
	}

	void moverFrase()
	{
		if (transform.eulerAngles.z > 50.0f && transform.eulerAngles.z  < 349.0f && ejeZ == 1)
			ejeZ = -1;
		if (transform.eulerAngles.z > 51.0f && transform.eulerAngles.z  < 350.0f && ejeZ == -1)
			ejeZ = 1;
		if (transform.localPosition.x > 300.0f)
			ejeX = 1;
		if (transform.localPosition.x < -300.0f)
			ejeX = -1;
		if (transform.localPosition.y > 300.0f)
			ejeY = 1;
		if (transform.localPosition.y < -300.0f)
			ejeY = -1;

		Vector3 rotation = transform.localEulerAngles;
		rotation.z = rotation.z +( ejeZ * 10 * Time.deltaTime);
		transform.localEulerAngles = rotation;

		transform.Translate (Vector3.left * ejeX * Time.deltaTime);
		transform.Translate (Vector3.down * ejeY * Time.deltaTime);

	}

	public void asignarValores(Reto3_ODA01 script, bool valor, GameObject objeto)
	{
		retoScript = script;
		basura = valor;
		apuntador = objeto;

		string last = this.name.Substring (this.name.Length-1);
		int lastDigit = int.Parse (last);
		if (lastDigit % 2 == 0) {
			ejeX = 1;
			ejeY = 1;
			ejeZ = 1;
		} else {
			ejeX = -1;
			ejeY = -1;
			ejeZ = -1;
		}
	}

	public bool GetBasura()
	{
		return basura;
	}

	public void OnBeginDrag(PointerEventData eventData)
	{

		//Se agrupa con el cursor
		if(gameObject.GetComponent<CanvasGroup>() == null){
			gameObject.AddComponent<CanvasGroup>();
		}

		//Se indica que no estorbe los ray casts
		gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;

		retoScript.genericUIScript.soundManager.playSound ("Sound_Scaner",1);
		retoScript.desactivarFrasesEscaner ();
		apuntador.SetActive (true);
		dragging = true;
	}

	public void OnDrag(PointerEventData eventData)
	{
		Vector3 globalMousePos =  new Vector3();
		if (eventData != null) {
			RectTransformUtility.ScreenPointToWorldPointInRectangle (
				eventData.pointerEnter.transform as RectTransform, eventData.position, 
				eventData.pressEventCamera, out globalMousePos
			);
		}
			
		transform.position = globalMousePos;
		apuntador.transform.position = globalMousePos;

		retoScript.activarFrasesEscaner (this.name);

		apuntador.SetActive (eventData.pointerDrag.activeSelf);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		apuntador.SetActive (false);
		dragging = false;
		retoScript.desactivarFrasesEscaner ();
		gameObject.GetComponent<CanvasGroup>().blocksRaycasts = true;
	}
}

public class DropReto3 : MonoBehaviour, IDropHandler
{
	Reto3_ODA01 retoScript;

	public void asignarValores(Reto3_ODA01 script)
	{
		retoScript = script;
	}

	public void OnDrop(PointerEventData data)
	{
		retoScript.validarFraseCorrecta (data);
	}
}