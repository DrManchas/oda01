﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Inicio_ODA01 : MonoBehaviour {

	//Pantallas
	GameObject inicio, menu, genericUI;

	//Botones
	Button btnEntrar;

	// Use this for initialization
	void Start () {
	
		//Pantallas
		inicio = this.transform.FindChild("Inicio").gameObject;
		inicio.GetComponent<Canvas> ().worldCamera = Camera.main;
		menu = this.transform.FindChild("Menu").gameObject;
		menu.GetComponent<Canvas> ().worldCamera = Camera.main;
		genericUI = this.transform.FindChild("GenericUI").gameObject;
		genericUI.GetComponent<Canvas> ().worldCamera = Camera.main;

		//Boton
		btnEntrar = inicio.transform.FindChild("btnEntrar").GetComponent<Button>();

		//Funciones boton
		btnEntrar.onClick.AddListener (delegate() {
			abrirMenu();
		});

	}

	void abrirMenu(){
		inicio.SetActive (false);
		menu.SetActive (true);
		genericUI.SetActive (true);
		genericUI.transform.FindChild ("gameUI").gameObject.SetActive (false);
		genericUI.transform.FindChild ("gameUI").FindChild ("herramientas").gameObject.SetActive(false);
		genericUI.transform.FindChild ("imgBlockScreen").gameObject.SetActive(false);
	}

}
